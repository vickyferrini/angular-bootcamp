import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { ListaComponent } from './lista/lista.component';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './user.service';
import { DetailsComponent } from './details/details.component';
import { UtilsModule } from '../utils/utils.module'
import { AltaComponent } from './alta/alta.component';


@NgModule({
  declarations: [
    ListaComponent,
    DetailsComponent,
    AltaComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    HttpClientModule,
    UtilsModule
  ],
  providers: [
    UserService
  ]  

})
export class UsersModule { }
