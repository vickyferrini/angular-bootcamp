import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { User } from './user.models';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _httpClient: HttpClient) { } //nombre de variable + tipo de dato//
  getUsers(): Observable<User[]> {
    return this._httpClient.get<User[]>('https://jsonplaceholder.typicode.com/users')
  }


  getUser(id:number):Observable<User>{
    return this._httpClient.get<User>('https://jsonplaceholder.typicode.com/users/' + id);
  
  }

  postUser(user: User): Observable<User>{
   return this._httpClient.post<User> ('https://jsonplaceholder.typicode.com/users/', user)
  }
}