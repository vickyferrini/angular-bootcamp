import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { User } from '../user.models';
import { UserService } from '../user.service';

@Component({
  selector: 'app-alta',
  templateUrl: './alta.component.html',
  styleUrls: ['./alta.component.css']
})
export class AltaComponent implements OnInit {
  form: FormGroup = new FormGroup({
    'id': new FormControl(),
    'name': new FormControl(),
    'username': new FormControl(),
    'phone': new FormControl()
  })

  user: User = {} as User;

  constructor(private _userService: UserService) { }

  ngOnInit(): void {
  }

  nuevoUsuario() {
    if (this.form.valid) {
      this.user.id = this.form.controls['id'].value;
      this.user.name = this.form.controls['name'].value;
      this.user.username = this.form.controls['username'].value;
      this.user.phone = this.form.controls['phone'].value;
      this._userService.postUser(this.user).subscribe({
        next: (x) => {
          console.log(x);
        },
        error: err => {
          console.log(err)
        }
      });
    }
    else {
      console.log('Falto completar el formulario');
    }
  }
}
